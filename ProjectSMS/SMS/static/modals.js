$(document).ready(function(){
		$(".optionDodajKlase").click(function(){
			$("#dodajKlaseModal").modal();
		});

		$(".optionDodajProfil").click(function(){
			$("#dodajProfilModal").modal();
		});

		$(".optionDodajUcznia").click(function(){
			$("#dodajUczniaModal").modal();
		});

		$(".optionDodajAlgorytm").click(function(){
			$("#dodajAlgorytmModal").modal();
		});

		$(".optionWypelnijKlase").click(function(){
			$("#wypelnijKlaseModal").modal();
		});

		$(".optionZrobKopie").click(function(){
			$("#zrobKopieModal").modal();
		});
		
		$(".optionAutomate").click(function(){
			$("#automateModal").modal();
		});

		$("#classIndexOF").modal();

		$("#error").slideDown(500,function(){})

		$("#saferRemoveCopyButton").click(function(){
			result = 0;
			result = confirm("Jesteś pewny ?\nTa akcja bezpowrotnie usunie kopie zapasową.");
			if(result)
				$("#finalRemoveCopyButton").click();
			});

		$("#saferRestoreCopyButton").click(function(){
			result = 0;
			result = confirm("Jesteś pewny ?\n\nTa akcja bezpowrotnie przyróci kopie zapasową.\nPowrót do aktualnej nie bedzie już możliwy jeżeli jej stan nie został zapisany !");
			if(result)
				$("#finalRestoreCopyButton").click();
			});

		$("#saferAutomateActionButton").click(function(){
			result = 0;
			result = confirm("Jesteś pewny ?\nTa akcja bezpowrotnie wykona operacje na bazie danych.");
			if(result)
				$("#finalAutomateActionButton").click();
			});

		$("#btn-safe-delete-class-yes").click(function(){
			var elem = document.getElementById("btn-safe-delete-class-yes");
			klasa = elem.dataset.delete;
			window.location.replace("/sms/extended/delklasa/"+klasa);
		});

		$("#btn-safe-delete-class-no").click(function(){
			$(".close").click();
		});

		$("#info-sign-optymalizuj").mouseenter(function(){
			$("#info-optymalizuj").stop();
			$("#info-optymalizuj").clearQueue();
			$("#info-optymalizuj").fadeIn("slow", function(){
				$("#info-optymalizuj").css({
					"display" : "block",
					"opacity" : "1",
				})
			});
		});
		
		$("#info-sign-optymalizuj").mouseout(function(){
			$("#info-optymalizuj").stop();
			$("#info-optymalizuj").clearQueue();
			$("#info-optymalizuj").fadeOut("slow", function(){});
		});

		$("#info-sign-fillclass").mouseenter(function(){
			$("#info-fillclass").stop();
			$("#info-fillclass").clearQueue();
			$("#info-fillclass").fadeIn("slow", function(){
				$("#info-fillclass").css({
					"display" : "block",
					"opacity" : "1",
				})
			});
		});
		
		$("#info-sign-fillclass").mouseout(function(){
			$("#info-fillclass").stop();
			$("#info-fillclass").clearQueue();
			$("#info-fillclass").fadeOut("slow", function(){});
		});

		$("#info-sign-addStudent").mouseenter(function(){
			$("#info-addStudent").stop();
			$("#info-addStudent").clearQueue();
			$("#info-addStudent").fadeIn("slow", function(){
				$("#info-addStudent").css({
					"display" : "block",
					"opacity" : "1",
				})
			});
		});
		
		$("#info-sign-addStudent").mouseout(function(){
			$("#info-addStudent").stop();
			$("#info-addStudent").clearQueue();
			$("#info-addStudent").fadeOut("slow", function(){});
		});


	});

function saferDeleteClass(klasa){
	var elem = document.getElementById("paragraphSafeDeleteClass");
	elem.innerHTML = "Potwierdzenie tej akcji spowoduję bezpowrotne usunięcie klasy " + klasa;
	elem = document.getElementById("btn-safe-delete-class-yes");
	elem.dataset.delete = klasa;
	$("#saferDeleteClassModal").modal();
} 
