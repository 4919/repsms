from django import forms
from django.core.validators import RegexValidator
alphanumericValidator = RegexValidator(r'\w{0,48}$', 'Tylko znaki alfanumeryczne sa dozwolone.')
numericValidator = RegexValidator(r'^[0-9]*$', 'Tylko cyfry sa dozwolone.')

class addStudent(forms.Form):
	imie = forms.CharField(label='Imie', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text",
		'class':"form-control",
		'id':'imieUcznia',
		'size':'20',
		'placeholder':"np. Paweł"}),
		validators=[alphanumericValidator]
		
	)

	nazwisko = forms.CharField(label='Nazwisko', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'nazwiskoUcznia',
		'class':"form-control",
		'size':'20',
		'placeholder':"np. Sagan"}),
		validators=[alphanumericValidator]
		
	)

	pesel = forms.CharField(label='Pesel', max_length=12, widget=forms.TextInput(
		attrs={
		'type':'text',
		'id':'peselUcznia',
		'class':'form-control',
		'size':'20',
		'placeholder':'11 cyfr'
		}),
		validators=[numericValidator]
	)

	# ADRES ZAMIESZKANIA
	kod1 = forms.CharField(widget=forms.TextInput(attrs={'id':'kod_p1','class':'kodp form-control','maxlength':'2','size':'1'}))
	kod2 = forms.CharField(widget=forms.TextInput(attrs={'id':'kod_p2','class':'kodp form-control','maxlength':'3','size':'2'}))

	miejscowosc = forms.CharField(max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'miejscowosc',
		'class':"form-control",
		'size':'20',
		'placeholder':"Miejscowosc"}),
		validators=[alphanumericValidator]
		
	)

	ulica = forms.CharField(max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ulica',
		'class':"form-control",
		'size':'14',
		'placeholder':"ulica"}),
		validators=[alphanumericValidator]
		
	)

	nrbudynku = forms.CharField(label='Nr.bud', required=False, max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'nrbud',
		'class':"form-control",
		'size':'4',
		'placeholder':"Nr.bud"}),
		validators=[numericValidator]
		
	)
	nrmieszkania = forms.CharField(label='Nr.miesz', required=False, max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'nrmiesz',
		'class':"form-control",
		'size':'5',
		'placeholder':"Nr.miesz"}),
		validators=[numericValidator]
		
	)

	# ADRES ZAMELDOWANIA
	kod12 = forms.CharField(widget=forms.TextInput(attrs={'id':'kod_p21','class':'kodp form-control','maxlength':'2','size':'1'}),
		validators=[numericValidator])
	kod22 = forms.CharField(widget=forms.TextInput(attrs={'id':'kod_p22','class':'kodp form-control','maxlength':'3','size':'1'}),
		validators=[numericValidator])

	miejscowosc2 = forms.CharField(max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'miejscowosc2',
		'class':"form-control",
		'size':'20',
		'placeholder':"Miejscowosc"}),
		validators=[alphanumericValidator]
		
	)

	ulica2 = forms.CharField(max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ulica2',
		'class':"form-control",
		'size':'14',
		'placeholder':"ulica"}),
		validators=[alphanumericValidator]
		
	)

	nrbudynku2 = forms.CharField(label='Nr.bud', required=False, max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'nrbud2',
		'class':"form-control",
		'size':'4',
		'placeholder':"Nr.bud"}),
		validators=[numericValidator]
		
	)

	nrmieszkania2 = forms.CharField(label='Nr.miesz', required=False, max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'nrmiesz2',
		'class':"form-control",
		'size':'5',
		'placeholder':"Nr.miesz"}),
		validators=[numericValidator]
		
	)

	ocenPol = forms.CharField(label='Ocena Polski', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ocenPol',
		'class':"form-control",
		'size':'1'}),
		validators=[numericValidator]
		
	)

	ocenMat = forms.CharField(label='Ocena Matematyka', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ocenMat',
		'class':"form-control",
		'size':'1'}),
		validators=[numericValidator]
		
	)

	ocenAng = forms.CharField(label='Ocena Angielski', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ocenAng',
		'class':"form-control",
		'size':'1'}),
		validators=[numericValidator]
		
	)

	ocenNiem = forms.CharField(label='Ocena Niemiecki', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ocenNiem',
		'class':"form-control",
		'size':'1'}),
		validators=[numericValidator]
		
	)

class formEditStudent(forms.Form):
	imie = forms.CharField(label='Imie', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'class':"form-control",
		'id':'imieUcznia',
		'size':'20',
		'placeholder':"np. Paweł"}),
		validators=[alphanumericValidator]
		
	)

	nazwisko = forms.CharField(label='Nazwisko', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'nazwiskoUcznia',
		'class':"form-control",
		'size':'20',
		'placeholder':"np. Sagan"}),
		validators=[alphanumericValidator]
		
	)
	
	pesel = forms.CharField(label='Pesel', max_length=11, widget=forms.TextInput(
		attrs={
		'type':'text',
		'id':'peselUcznia',
		'class':'form-control',
		'size':'20',
		'placeholder':'11 cyfr'
		}),
		validators=[numericValidator]
	)

	# ADRES ZAMIESZKANIA
	kod1 = forms.CharField(widget=forms.TextInput(attrs={'id':'kod_p1','class':'kodp','maxlength':'2','size':'1'}),
		validators=[numericValidator])
	kod2 = forms.CharField(widget=forms.TextInput(attrs={'id':'kod_p2','class':'kodp','maxlength':'3','size':'2'}),
		validators=[numericValidator])

	miejscowosc = forms.CharField(max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'miejscowosc',
		'class':"form-control",
		'size':'20',
		'placeholder':"Miejscowosc"}),
		validators=[alphanumericValidator]
		
	)

	ulica = forms.CharField(max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ulica',
		'class':"form-control",
		'size':'20',
		'placeholder':"ulica"}),
		validators=[alphanumericValidator]
		
	)

	nrbudynku = forms.CharField(label='Nr.bud', required=False, max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'nrbud',
		'class':"form-control",
		'size':'4',
		'placeholder':"Nr.bud"}),
		validators=[alphanumericValidator]
		
	)
	nrmieszkania = forms.CharField(label='Nr.miesz', required=False, max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'nrmiesz',
		'class':"form-control",
		'size':'5',
		'placeholder':"Nr.miesz"}),
		validators=[alphanumericValidator]
		
	)

	# ADRES ZAMELDOWANIA
	kod12 = forms.CharField(widget=forms.TextInput(attrs={'id':'kod_p21','class':'kodp','maxlength':'2','size':'1'}),
		validators=[numericValidator])
	kod22 = forms.CharField(widget=forms.TextInput(attrs={'id':'kod_p22','class':'kodp','maxlength':'3','size':'1'}),
		validators=[numericValidator])

	miejscowosc2 = forms.CharField(max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'miejscowosc2',
		'class':"form-control",
		'size':'20',
		'placeholder':"Miejscowosc"}),
		validators=[alphanumericValidator]
		
	)

	ulica2 = forms.CharField(max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ulica2',
		'size':'20',
		'class':"form-control",
		'placeholder':"ulica"}),
		validators=[alphanumericValidator]
		
	)

	nrbudynku2 = forms.CharField(label='Nr.bud', required=False, max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'nrbud2',
		'size':'4',
		'class':"form-control",
		'placeholder':"Nr.bud"}),
		validators=[numericValidator]
		
	)

	nrmieszkania2 = forms.CharField(label='Nr.miesz', required=False, max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'nrmiesz2',
		'size':'5',
		'class':"form-control",
		'placeholder':"Nr.miesz"}),
		validators=[numericValidator]
		
	)

	klasa = forms.ChoiceField(label='klasa')

	ocenPol = forms.CharField(label='Ocena Polski', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ocenPol',
		'class':"form-control",
		'size':'1'}),
		validators=[numericValidator]
		
	)

	ocenMat = forms.CharField(label='Ocena Matematyka', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ocenMat',
		'class':"form-control",
		'size':'1'}),
		validators=[numericValidator]
		
	)

	ocenAng = forms.CharField(label='Ocena Angielski', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ocenAng',
		'class':"form-control",
		'size':'1'}),
		validators=[numericValidator]
		
	)

	ocenNiem = forms.CharField(label='Ocena Niemiecki', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'ocenNiem',
		'class':"form-control",
		'size':'1'}),
		validators=[numericValidator]
		
	)

	iducznia = forms.CharField(max_length=32, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'id':'iducznia',
		'class':"form-control hidden",
		}),
		validators=[numericValidator]
		
	)

	def __init__(self,*args, **kwargs):
		klasyVar = kwargs.pop('klasy', None)
		super(formEditStudent, self).__init__(*args, **kwargs)
		self.fields['klasa'].choices = klasyVar

class removeAlgorithm(forms.Form):
	algorithm = forms.ChoiceField(label='Usun')

	def __init__(self,*args, **kwargs):
		algorithmVar = kwargs.pop('algorytm', None)
		super(removeAlgorithm, self).__init__(*args, **kwargs)
		self.fields['algorithm'].choices = algorithmVar

class addClass(forms.Form):
	nazwaKlasy = forms.CharField(label='Nazwa klasy', max_length=48, widget=forms.TextInput(
		attrs={
		'type':"text", 
		'class':"form-control",
		'id':'nazwaKlasy',
		'placeholder':"nazwa"}),
		validators=[alphanumericValidator]
		
	)
	liczebnosc = forms.CharField(label='Liczebnosc klasy',widget=forms.TextInput(
		attrs={
		'type':"text", 
		'class':"form-control",
		'placeholder':"np. 30"}),
		validators=[numericValidator]
	)
	algorytm = forms.ChoiceField(label='Algorytm')

	def __init__(self,*args, **kwargs):
		algorytmVar = kwargs.pop('algorytm', None)
		super(addClass, self).__init__(*args, **kwargs)
		self.fields['algorytm'].choices = algorytmVar

class addAlgorithm(forms.Form):
	nazwa = forms.CharField(label='Nazwa algorytmu',max_length=48,widget=forms.TextInput(
		attrs={
		'type':"text",
		'class':"form-control",
		'size':'20'}),
		validators=[alphanumericValidator]
	)

	matematyka = forms.IntegerField(label='Matematyka',widget=forms.TextInput(
		attrs={
		'type':'number',
		'min':'0',
		'class':'form-control',
		'size':'20'}),
		validators=[numericValidator]
	)

	jpolski = forms.IntegerField(label='Język polski',widget=forms.TextInput(
		attrs={
		'type':'number',
		'min':'0',
		'class':"form-control",
		'size':'20'}),
		validators=[numericValidator]
	)

	jangielski = forms.IntegerField(label='Język Angielski',widget=forms.TextInput(
		attrs={
		'type':'number',
		'min':'0',
		'class':"form-control",
		'size':'20'}),
		validators=[numericValidator]
	)

	jniemiecki = forms.IntegerField(label='Język Niemiecki',widget=forms.TextInput(
		attrs={
		'type':'number',
		'min':'0',
		'class':"form-control",
		'size':'20'}),
		validators=[numericValidator]
	)

class fillClass(forms.Form):
	

	nazwaKlasy = forms.CharField(label='Nazwa klasy',required=True,max_length=48,widget=forms.TextInput(
				attrs={
				'class':'form-control'
				}),validators=[alphanumericValidator])
	algorytmy = forms.ChoiceField(label='Algorytm')
	sposob = forms.BooleanField(label='Optymalizuj',required=False)
	wielkosc = forms.IntegerField(label='Wielkość klasy',required=True,min_value=1,widget=forms.NumberInput(
				attrs={
				'class':'form-control'
				}))
	limit = forms.IntegerField(label='Maks. ilość klas',required=False,min_value=0,widget=forms.NumberInput(
				attrs={
				'class':'form-control'
				}))

	def __init__(self,*args, **kwargs):
		algoVar = kwargs.pop('algorytmy',None)
		super(fillClass, self).__init__(*args, **kwargs)
		self.fields['algorytmy'].choices = algoVar

class removeCopy(forms.Form):
	nazwaKopii = forms.ChoiceField(label = '',widget=forms.Select(attrs={'class':'selectUsunKopie'}))

	def __init__(self,*args, **kwargs):
		kopieVar = kwargs.pop('kopie', None)
		super(removeCopy, self).__init__(*args, **kwargs)
		self.fields['nazwaKopii'].choices = kopieVar

class makeCopy(forms.Form):
	nazwaKopii = forms.CharField(label='',max_length=48, widget=forms.TextInput(
		attrs={
		'type':'text',
		'class':"form-control",
		'size':'13',
		'placeholder':'Nazwa kopii'
		}),
		validators=[alphanumericValidator]
	)

class restoreCopy(forms.Form):
	nazwaKopii = forms.ChoiceField(label = '',widget=forms.Select(attrs={'class':'selectUsunKopie'}))

	def __init__(self,*args, **kwargs):
		kopieVar = kwargs.pop('kopie', None)
		super(restoreCopy, self).__init__(*args, **kwargs)
		self.fields['nazwaKopii'].choices = kopieVar

class autoAction(forms.Form):
	actions = (
			("deleteStudent","USUŃ UCZNIA"),
			#("fillClass","WYPEŁNIJ KLASĘ"),
			)

	reference = forms.ChoiceField(label = 'dotyczy')

	action = forms.ChoiceField(label = 'Akcja', choices = actions)

	def __init__(self, *args, **kwargs):
		refs = kwargs.pop('klasy', None)
		super(autoAction, self).__init__(*args, **kwargs)
		self.fields['reference'].choices = refs
