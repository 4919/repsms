from django.shortcuts import render, redirect
from django.http import Http404, HttpResponseRedirect
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.db import transaction
from .forms import addStudent, addClass, addAlgorithm,  removeAlgorithm, fillClass, formEditStudent, removeCopy, makeCopy, restoreCopy, autoAction
from .funkcjeopty import dejnumer, optymalizuj, rest
from .fillfuncs import fillclasses_sqlDict
from .vec import vectorContains, choiceFieldTupleContains
from .errorHandling import handleError
import os
import sqlite3
import math
import datetime
from .csvfuncs import searchcsv, importcsv
from django.contrib.auth import logout
from operator import itemgetter
from .dbfuncs import sqlDict, sqlDict_sort
from shutil import copyfile

@login_required
@transaction.atomic
def smsApp(request):

	if request.user.is_authenticated:

		if  request.user.is_expired():
			logout(request)
			HttpResponseRedirect('/')

		current_user = request.user

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		dbname = current_user.username
		dbname += ".sqlite3"
		conn = sqlite3.connect(os.path.join(BASE_DIR,'userData',current_user.username + '.sqlite3'))

		c = conn.cursor()

		# Preparing data to send it to forms
		# Algorithms choicefield
		# Need to do this that way because of the manner of django functions.
		# forms.choicefield gets an array that contains only 2 indexes
		# first one contains name and the secound value of the select tag that is rendered by mentioned function
		c.execute("SELECT * FROM algorytmy WHERE nazwa!='Brak'")
		algorithms = c.fetchall()
		algorithmspass = []
		for cols in algorithms:
			algorithmspass.append(cols[0:2])

		#Im getting class names from db from every student
		#then every unique class is appended to klasyPostfix which will contain
		#list of all existing classes
		uczniowie = sqlDict(c, "SELECT * FROM uczniowie ORDER BY klasa")

		# Jinja doesnt like my sqlDict
		# To be changed in future
		allStudents = c.execute("SELECT * FROM uczniowie ORDER BY klasa, Nazwisko")
		allStudents = allStudents.fetchall()

		klasyPostfix = []

		for klasa in uczniowie['klasa']:
			if( not( vectorContains(klasyPostfix,klasa) ) and klasa != None ):
				klasyPostfix.append(klasa)


		dbCopies = []
		for name in os.listdir(os.path.join(BASE_DIR,'userData','copy' , current_user.username, '.')):
			if os.path.isfile(os.path.join(BASE_DIR , 'userData','copy', current_user.username, name)):
				dbCopies.append(name)

		i=0
		dbCopiesPass = []
		while(i < len(dbCopies)):
			dbCopiesPass.append([dbCopies[i],dbCopies[i]])
			i+=1

		# Django want as choice list for select packed data as tuple like that:
		# (valuePassedToScript,valueDisplayedInHtml)
		# this is basically redundant data even twice, however have no better idea for now
		klasyFixex = []
		klasyFixex.append(("Brak","Brak"))
		klasyFixex.append(("All","Wszystkich - również bez klasy"))
		for each in klasyPostfix:
			klasyFixex.append((each,each))


		formAddStudent = addStudent
		formAddAlgorithm = addAlgorithm
		formRemoveAlgorithm = removeAlgorithm(algorytm=(algorithmspass))
		#algorithmspass.insert(0,("Brak","Brak")) # So removeAlgoForm cant remove default algorithm
		formFillClass = fillClass(algorytmy=(algorithmspass))
		formRemoveCopy = removeCopy(kopie=(dbCopiesPass))
		formMakeCopy = makeCopy()
		formRestoreCopy = restoreCopy(kopie=(dbCopiesPass))
		formautoAction = autoAction(klasy=(klasyFixex))

		#Gets the value of session variable that fillclass will set to 1 if overflow in class index occurs
		#So the value will be passed once to the jinja to display notification
		fillclassOF = request.session.get('SfillClassOF')
		request.session['SfillClassOF'] = 0

		#Gets the value of session variable that makeCopy sets to 1 if there is already
		#a copy of database with name that user inserted
		resultMakeCopy = request.session.get('resultMakeCopy')
		request.session['resultMakeCopy'] = 0

		#Every request.POST catcher appends to this variable its errors if any occur
		messages = request.session.get('errors')
		request.session['errors'] = []

		context={
			"current_user":current_user,
			"formAddStudent":formAddStudent,
			"formAddAlgorithm":formAddAlgorithm,
			"formRemoveAlgorithm":formRemoveAlgorithm,
			"formFillClass":formFillClass,
			"klasyPostfix":klasyPostfix,
			"fillclassOF":fillclassOF,
			"formRemoveCopy":formRemoveCopy,
			"formMakeCopy":formMakeCopy,
			"formRestoreCopy":formRestoreCopy,
			"formAutomateAction":formautoAction,
			"resultMakeCopy":resultMakeCopy,
			"allStudents": allStudents,
			"copyCount": current_user.copyCount,
			"dbCopies":dbCopies,
			"messages":messages,
		}

		if request.method == 'POST':
			# Dodawanie ucznia, pojedyncze
			if "addStudent" in request.POST:
				formAddStudent = addStudent(request.POST)
				if formAddStudent.is_valid():
					#There was one csv that instead of blank fields had actual 'null' string in it
					#It causes some problems in fill / opt functions
					for key,value in formAddStudent.cleaned_data.items():
						if(value == 'null'):
							formAddStudent.cleaned_data[key] = 0

					imieUcznia = formAddStudent.cleaned_data['imie']
					nazwiskoUcznia = formAddStudent.cleaned_data['nazwisko']
					peselUcznia = formAddStudent.cleaned_data['pesel']
					kod1Ucznia = formAddStudent.cleaned_data['kod1']
					kod2Ucznia = formAddStudent.cleaned_data['kod2']
					kodUcznia1 = kod1Ucznia +'-'+ kod2Ucznia
					miejscowoscUcznia = formAddStudent.cleaned_data['miejscowosc']
					ulicaUcznia = formAddStudent.cleaned_data['ulica']
					nrbudynkuUcznia = formAddStudent.cleaned_data['nrbudynku']
					nrmieszkaniaUcznia = formAddStudent.cleaned_data['nrmieszkania']
					kod12Ucznia = formAddStudent.cleaned_data['kod12']
					kod22Ucznia = formAddStudent.cleaned_data['kod22']
					kodUcznia2 = kod12Ucznia +'-'+ kod22Ucznia
					miejscowosc2Ucznia = formAddStudent.cleaned_data['miejscowosc2']
					ulica2Ucznia = formAddStudent.cleaned_data['ulica2']
					nrbudynku2Ucznia = formAddStudent.cleaned_data['nrbudynku2']
					nrmieszkania2Ucznia = formAddStudent.cleaned_data['nrmieszkania2']
					ocenaPolskiUcznia = formAddStudent.cleaned_data['ocenPol']
					ocenaMatematykaUcznia = formAddStudent.cleaned_data['ocenMat']
					ocenaAngielskiUcznia = formAddStudent.cleaned_data['ocenAng']
					ocenaNiemieckiUcznia = formAddStudent.cleaned_data['ocenNiem']

					c = conn.cursor()
					c.execute("INSERT INTO uczniowie (Imię,Nazwisko,Pesel,Kod_pocztowy,Miejscowość,Ulica,Nr_budynku,Nr_mieszkania,Kod_pocztowy2,Miejscowość2,Ulica2,Nr_budynku2,Nr_mieszkania2,polski,matematyka,angielski,niemiecki) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(imieUcznia,nazwiskoUcznia,peselUcznia,kodUcznia1,miejscowoscUcznia,ulicaUcznia,nrbudynkuUcznia,nrmieszkaniaUcznia,kodUcznia2,miejscowosc2Ucznia,ulica2Ucznia,nrbudynku2Ucznia,nrmieszkania2Ucznia,ocenaMatematykaUcznia,ocenaPolskiUcznia,ocenaAngielskiUcznia,ocenaNiemieckiUcznia))
					conn.commit()
					conn.close()

					context.update({"formAddStudent":formAddStudent})
					return HttpResponseRedirect("/sms/extended")
				else:
					handleError(current_user.username,formAddStudent)
					request.session['errors'].append("Dane wprowadzone niepoprawnie.")
					context.update({"formAddStudent":formAddStudent})
					return HttpResponseRedirect("/sms/extended")

					# Wypelnianie klasy

			elif "fillClass" in request.POST:
				formFillClass = fillClass(request.POST, algorytmy=(algorithmspass))
				if formFillClass.is_valid():
					c = conn.cursor()
					nazwaKlasy = formFillClass.cleaned_data['nazwaKlasy']

					# Checks if class of that name already exists
					letter = 'A'
					while ord(letter) <= ord('Z'):
						try:
							c.execute("SELECT id FROM {}".format(nazwaKlasy+letter))
							q = c.fetchall()
							if q != []:
								handleError(current_user.username,formFillClass)
								request.session['errors'].append("Klasa o takiej nazwie już istnieje.")
								context.update({"formFillClass":formFillClass})
								return HttpResponseRedirect("/sms/extended")
						except:
							pass

						letter = chr(ord(letter) + 1)

					sposob = formFillClass.cleaned_data['sposob']
					limit  = formFillClass.cleaned_data['limit']
					algo = formFillClass.cleaned_data['algorytmy']
					liczebnosc = formFillClass.cleaned_data['wielkosc']
					klasa = {'algorytm':algo,'litera':'A','liczebnosc':liczebnosc,'nazwaKlasy':nazwaKlasy}
					#klasa = sqlDict(c, "SELECT * FROM klasy WHERE id=" + formFillClass.cleaned_data['klasy'])

					c.execute("SELECT * FROM uczniowie WHERE klasa IS NULL")

					uczniowie = c.fetchall()
					uczniowie = list(map(list,uczniowie))

					# start of implementation of opti functions from php
					ile = len(uczniowie)
					if(ile>=1 and sposob==True):
						odp = []
						odpowiedzi = [] # I have no idea why i chose similar names. Have no time to figure it out right now
						sizeofclass = int(klasa['liczebnosc'])
						odpowiedzi.append(dejnumer(ile,sizeofclass)) # First argument is size of all students to sort out, secound is size of class
						odp.append(optymalizuj(odpowiedzi[0],sizeofclass)) # Same arguments as above here
						odp[0] = rest(odp[0]) # Here as argument I need returned value from optymalizuj function
						uczniowie = sqlDict(c,"SELECT * FROM uczniowie WHERE klasa IS NULL")
						fillclassOF = fillclasses_sqlDict(c,conn,klasa,uczniowie,current_user,odp, limit) #fillclasses_sqlDict returns 0 when no errors occur and 1 when index to class is above 'Z'

					else:
						uczniowie = sqlDict(c,"SELECT * FROM uczniowie WHERE klasa IS NULL")
						fillclassOF = fillclasses_sqlDict(c,conn,klasa,uczniowie,current_user,0, limit) #fillclasses_sqlDict returns 0 when no errors occur and 1 when index to class is above 'Z'
					# end of implementatioformFillClass.errors

					#setting the session variable to return value of fillclasses_sqlDict funtion
					#It will trigger the pop-up in jinja to display message to user that index in class have overflowed
					request.session['SfillClassOF'] = fillclassOF

					context.update({"formFillClass":formFillClass})
					return HttpResponseRedirect("/sms/extended")
				else:
					handleError(current_user.username,formFillClass)
					request.session['errors'].append("Dane wprowadzone niepoprawnie.")
					context.update({"formFillClass":formFillClass})
					return HttpResponseRedirect("/sms/extended")

			# Dodawanie ucznia / uczniow poprzez plik

			elif "studentFile" in request.POST:

				#Tablica szukanych kolumn w pliku csv

				wantedtable = [
					"Imię",
					"Pesel",
					"Nazwisko",
					"Kod pocztowy",
					"Miejscowość",
					"Ulica",
					"Nr budynku",
					"Nr mieszkania",
					"polski",
					"angielski",
					"niemiecki",
					"francuski",
					"wloski",
					"hiszpanski",
					"rosyjski",
					"matematyka",
					"fizyka",
					"informatyka",
					"historia",
					"biologia",
					"chemia",
					"geografia",
					"wos",
					"zajęcia techniczne",
					"zajęcia artstyczne",
					"edukacja dla bezpieczeństwa",
					"plastyka",
					"muzyka",
					"wf",
					"zachowanie",
					]

				answer = []
				c = conn.cursor()

				for word in wantedtable:
					temp = searchcsv(word,request.FILES['studentFile'])
					if(temp != None):
						answer.append(searchcsv(word,request.FILES['studentFile']))
					else:
						answer.append('')

				importcsv(wantedtable,answer,request.FILES['studentFile'],c)

				conn.commit()
				conn.close()

				return HttpResponseRedirect("/sms/extended")

			# Dodawanie algorytmu
			elif "addAlgorithm" in request.POST:
				formAddAlgorithm = addAlgorithm(request.POST)
				if formAddAlgorithm.is_valid():
					nazwaAlgo = formAddAlgorithm.cleaned_data['nazwa']
					matematyka = formAddAlgorithm.cleaned_data['matematyka']
					jpolski = formAddAlgorithm.cleaned_data['jpolski']
					jangielski = formAddAlgorithm.cleaned_data['jangielski']
					jniemiecki = formAddAlgorithm.cleaned_data['jniemiecki']

					# Checks if input from user are positive integers
					isInputNegative = 0
					iterPostData = iter(formAddAlgorithm.cleaned_data)
					next(iterPostData)
					for entry in iterPostData:
						if formAddAlgorithm.cleaned_data[entry] < 0:
							isInputNegative += 1

					if isInputNegative == 0:
						c = conn.cursor()
						c.execute("CREATE TABLE IF NOT EXISTS algorytmy(id integer NOT NULL PRIMARY KEY AUTOINCREMENT, nazwa text, jpolski integer, matematyka integer, jangielski integer, jniemiecki integer)")
						c.execute("SELECT * FROM algorytmy")
						same = False
						for row in c.fetchall():
							if (row[1] == nazwaAlgo):
								same = True
								break
						if(same == False):
							c.execute("INSERT INTO algorytmy VALUES(?,?,?,?,?,?)",(None,nazwaAlgo,matematyka,jpolski,jangielski,jniemiecki))
						conn.commit()
						conn.close()

						context.update({"formAddAlgorithm":formAddAlgorithm})
						return HttpResponseRedirect("/sms/extended")
					else:
						handleError(current_user.username,formAddAlgorithm)
						request.session['errors'].append("Dane wprowadzone niepoprawnie.")
						context.update({"formAddAlgorithm":formAddAlgorithm})
						return HttpResponseRedirect("/sms/extended")

					# Usuwanie profilu

			elif "removeAlgorithm" in request.POST:
				formRemoveAlgorithm =  removeAlgorithm(request.POST,algorytm=(algorithmspass))
				if formRemoveAlgorithm.is_valid():
					algoName = formRemoveAlgorithm.cleaned_data['algorithm']
					c = conn.cursor()
					query = ""
					query = "DELETE FROM algorytmy WHERE id =" + algoName
					c.execute(query)
					conn.commit()
					conn.close()

					context.update({"formRemoveAlgorithm":formRemoveAlgorithm})
					return HttpResponseRedirect("/sms/extended")
				else:
					handleError(current_user.username,formRemoveAlgorithm)
					request.session['errors'].append("Coś poszło nie tak ;/")
					context.update({"formRemoveAlgorithm":formRemoveAlgorithm})
					return HttpResponseRedirect("/sms/extended")

			elif "removeCopy" in request.POST:
				formRemoveCopy =  removeCopy(request.POST,kopie=(dbCopiesPass))
				if formRemoveCopy.is_valid():
					nazwaKopii = formRemoveCopy.cleaned_data['nazwaKopii']

					# Someone uses dot in copyname
					pathTraversalTest = nazwaKopii.find(".")
					if(pathTraversalTest != -1):
						print("Pathtraversal attempt ! From user: " + current_user.username)
						context.update({"formMakeCopy":formMakeCopy})
						return HttpResponseRedirect("/sms/extended")

					# Someone uses double dots in copyname
					pathTraversalTest = nazwaKopii.find("..")
					if(pathTraversalTest != -1):
						print("Pathtraversal attemp ! From user: " + current_user.username)
						context.update({"formRemoveCopy":formRemoveCopy})
						return HttpResponseRedirect("/sms/extended")

					# Someone uses slashes in copyname
					pathTraversalTest = nazwaKopii.find("/")
					if(pathTraversalTest != -1):
						print("Pathtraversal attemp ! From user: " + current_user.username)
						context.update({"formRemoveCopy":formRemoveCopy})
						return HttpResponseRedirect("/sms/extended")

					# Someone uses slashes in copyname
					pathTraversalTest = nazwaKopii.find("\\")
					if(pathTraversalTest != -1):
						print("Pathtraversal attemp ! From user: " + current_user.username)
						context.update({"formRemoveCopy":formRemoveCopy})
						return HttpResponseRedirect("/sms/extended")

					# Empty input
					if(len(nazwaKopii) == 0):
						print("Empty input in pathtraversal check. From user: " + current_user.username)
						context.update({"formRemoveCopy":formRemoveCopy})
						return HttpResponseRedirect("/sms/extended")

					path = os.path.join(BASE_DIR, 'userData','copy' , current_user.username, nazwaKopii)
					try:
						os.remove(path)
					except:
						pass

					context.update({"formRemoveCopy":formRemoveCopy})
					return HttpResponseRedirect("/sms/extended")
				else:
					handleError(current_user.username,formRemoveCopy)
					request.session['errors'].append("Coś poszło nie tak ;/")
					context.update({"formRemoveCopy":formRemoveCopy})
					return HttpResponseRedirect("/sms/extended")

			elif "makeCopy" in request.POST:
				formMakeCopy = makeCopy(request.POST)
				if(formMakeCopy.is_valid()):
					nazwaKopii = formMakeCopy.cleaned_data['nazwaKopii']

					if(len(dbCopies) >= current_user.copyCount):
						request.session['resultMakeCopy'] = 2

					#Checks if the copy with that name already exists
					#If so, will set variable to 1 and pass it to jinja
					#Jinja will popup modal box with error

					for copy in dbCopies:
						if copy == nazwaKopii:
							request.session['resultMakeCopy'] = 1

					if(not request.session['resultMakeCopy']):

						# Someone uses dot in copyname
						pathTraversalTest = nazwaKopii.find(".")
						if(pathTraversalTest != -1):
							handleError(current_user.username,formMakeCopy)
							request.session['errors'].append("Coś poszło nie tak ;/")
							context.update({"formMakeCopy":formMakeCopy})
							return HttpResponseRedirect("/sms/extended")

						# Someone uses double dots in copyname
						pathTraversalTest = nazwaKopii.find("..")
						if(pathTraversalTest != -1):
							handleError(current_user.username,formMakeCopy)
							request.session['errors'].append("Coś poszło nie tak ;/")
							context.update({"formMakeCopy":formMakeCopy})
							return HttpResponseRedirect("/sms/extended")

						# Someone uses slashes in copyname
						pathTraversalTest = nazwaKopii.find("/")
						if(pathTraversalTest != -1):
							handleError(current_user.username,formMakeCopy)
							request.session['errors'].append("Coś poszło nie tak ;/")
							context.update({"formMakeCopy":formMakeCopy})
							return HttpResponseRedirect("/sms/extended")


						# Someone uses slashes in copyname
						pathTraversalTest = nazwaKopii.find("\\")
						if(pathTraversalTest != -1):
							handleError(current_user.username,formMakeCopy)
							request.session['errors'].append("Coś poszło nie tak ;/")
							context.update({"formMakeCopy":formMakeCopy})
							return HttpResponseRedirect("/sms/extended")

						# Empty input
						if(len(nazwaKopii) == 0):
							handleError(current_user.username,formMakeCopy)
							request.session['errors'].append("Nazwa nie może być pusta")
							context.update({"formMakeCopy":formMakeCopy})
							return HttpResponseRedirect("/sms/extended")

						src = os.path.join(BASE_DIR, 'userData', current_user.username + ".sqlite3" )
						dst = os.path.join(BASE_DIR, 'userData','copy', current_user.username, nazwaKopii)
						copyfile(src, dst)

					context.update({"formMakeCopy":formMakeCopy})
					return HttpResponseRedirect("/sms/extended")
				else:
					handleError(current_user.username,formMakeCopy)
					request.session['errors'].append("Coś poszło nie tak ;/")
					context.update({"formMakeCopy":formMakeCopy})
					return HttpResponseRedirect("/sms/extended")

			elif "restoreCopy" in request.POST:
				formRestoreCopy =  restoreCopy(request.POST,kopie=(dbCopiesPass))
				if formRestoreCopy.is_valid():
					nazwaKopii = formRestoreCopy.cleaned_data['nazwaKopii']
					dst = os.path.join(BASE_DIR, 'userData', current_user.username + ".sqlite3")
					src = os.path.join(BASE_DIR,'userData','copy', current_user.username , nazwaKopii)
					copyfile(src, dst)

					context.update({"formRestoreCopy":formRestoreCopy})
					return HttpResponseRedirect("/sms/extended")
				else:
					handleError(current_user.username,formRestoreCopy)
					request.session['errors'].append("Taka kopia nie istnieje")
					context.update({"formRestoreCopy":formRestoreCopy})
					return HttpResponseRedirect("/sms/extended")

			elif "autoAction" in request.POST:
				formautoAction = autoAction(request.POST, klasy=(klasyFixex))
				if formautoAction.is_valid():
					action = formautoAction.cleaned_data['action']
					ref = formautoAction.cleaned_data['reference']

					if action == 'deleteStudent':
						if ref == 'Brak':
							c.execute("DELETE FROM uczniowie WHERE klasa IS NULL")
						elif ref == 'All':
							c.execute("DELETE FROM uczniowie")
							for eachClass in klasyPostfix:
								c.execute("DROP TABLE {}".format(eachClass))
						else:
							c.execute("DELETE FROM uczniowie WHERE klasa='{}'".format(ref))
							c.execute("DROP TABLE {}".format(ref))
					elif action == 'fillClass':
						print("Wypelniam klase")

					conn.commit()
					conn.close()
					return HttpResponseRedirect("/sms/extended")
				else:
					handleError(current_user.username,formautoAction)
					request.session['errors'].append("Coś poszło nie tak ;/")
					context.update({"formautoAction":formautoAction})
					return HttpResponseRedirect("/sms/extended")

		return render (request, "SMS.html", context)

def showAllStudents(request):
	if request.user.is_authenticated:
		if  request.user.is_expired():
			logout(request)
			HttpResponseRedirect('/')

		current_user = request.user

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		dbname = current_user.username
		dbname += ".sqlite3"
		conn = sqlite3.connect(os.path.join(BASE_DIR,'userData',current_user.username + '.sqlite3'))
		c = conn.cursor()

		c.execute("SELECT * FROM uczniowie")
		allStudents = c.fetchall()
		headerTable = [description[0] for description in c.description]

		context={
			"allStudents":allStudents,
			"headerTable":headerTable,
		}

		return render (request, "allStudents.html",context)
def collectclasses(c,current_user):
	c.execute("SELECT * FROM klasy")
	classes = c.fetchall()
	className = []
	for everyclass in classes:
		letter = 'A'
		while(everyclass[4] >= letter):
			className.append(str(everyclass[0]) + letter)
			letter = ord(letter)
			letter += 1
			letter = chr(letter)
	return className

def showAllStudentsWithClass(request):
	if request.user.is_authenticated:
		if  request.user.is_expired():
			logout(request)
			HttpResponseRedirect('/')

		current_user = request.user

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		dbname = current_user.username
		dbname += ".sqlite3"
		conn = sqlite3.connect(os.path.join(BASE_DIR,'userData', current_user.username + '.sqlite3'))
		c = conn.cursor()

		c.execute("SELECT * FROM uczniowie")
		allStudents = c.fetchall()
		headerTable = [description[0] for description in c.description]

		classList = collectclasses(c,current_user)

		# THIS QUERY IS TEMPORARY !!! THIS CAN PROVIDE CRASHES IN THE FUTUTRE
		# Im getting all the fields with NULL
		# but this value for no class might change in the future
		# so im just targeting it worth looking sometime when crash occurs.
		classes = []
		for each in classList:
			query = "SELECT * FROM uczniowie WHERE klasa='"+each+"'"
			c.execute(query)
			result = c.fetchall()
			if result:
				classes.append(result)

		context={
			"classes":classes,
			"headerTable":headerTable,
		}

		return render (request, "studentsWithClass.html",context)

def showAllStudentsWithNoClass(request):
	if request.user.is_authenticated:
		if  request.user.is_expired():
			logout(request)
			HttpResponseRedirect('/')

		current_user = request.user

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		dbname = current_user.username
		dbname += ".sqlite3"
		conn = sqlite3.connect(os.path.os(BASE_DIR,'userData', current_user.username + '.sqlite3'))
		c = conn.cursor()

		c.execute("SELECT * FROM uczniowie WHERE klasa IS NULL")
		allStudents = c.fetchall()

		headerTable = [description[0] for description in c.description]

		context={
			"allStudents":allStudents,
			"headerTable":headerTable,
		}

		return render (request, "studentsWithNoClass.html", context)

def manageStudents(request):

	if request.user.is_authenticated:
		if  request.user.is_expired():
			logout(request)
			HttpResponseRedirect('/')

		current_user = request.user

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		dbname = current_user.username
		dbname += ".sqlite3"
		conn = sqlite3.connect(os.path.join(BASE_DIR, 'userData', current_user.username + '.sqlite3'))
		c = conn.cursor()

		c.execute("SELECT * FROM uczniowie")
		allStudents = c.fetchall()

		headerTable = [description[0] for description in c.description]

	context={
		"allStudents":allStudents,
		"headerTable":headerTable,
	}

	return render(request, "manageStudent.html", context)

def deleteStudentsFromClass(request, id):
	if request.user.is_authenticated:
		if  request.user.is_expired():
			logout(request)
			HttpResponseRedirect('/')

		current_user = request.user

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		dbname = current_user.username
		dbname += ".sqlite3"
		conn = sqlite3.connect(os.path.join(BASE_DIR,'userData',current_user.username + '.sqlite3'))
		c = conn.cursor()

		query = "SELECT * FROM uczniowie WHERE id="+id
		uczen = sqlDict(c, query)

		if(uczen['id'][0] == []):
			print("Error when deleting student")
			return redirect(smsApp)

		klasa = uczen['klasa'][0]

		#Usuwa ucznia z tabeli klasy do ktorej nalezy
		query = "DELETE FROM "+ klasa +" WHERE iducznia="+id
		c.execute(query)
		conn.commit()

		#Usuwa wartosc z kolumny klasa usuniętgo ucznia
		query = "UPDATE uczniowie SET klasa = NULL WHERE id="+id
		c.execute(query)
		conn.commit()
		conn.close()

	return redirect(showSpecificClass, klasa)

def showSpecificClass(request, klasa):
	if request.user.is_authenticated:
		if  request.user.is_expired():
			logout(request)
			HttpResponseRedirect('/')

		current_user = request.user

		szczegoly = request.GET.get('szczegoly','')

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		dbname = current_user.username
		dbname += ".sqlite3"
		conn = sqlite3.connect(os.path.join(BASE_DIR, 'userData', current_user.username + '.sqlite3'))
		c = conn.cursor()

		query = "SELECT * FROM uczniowie WHERE klasa='"+klasa+"'"
		c.execute(query)
		allStudents = c.fetchall()
		headerTable = [description[0] for description in c.description]

		headerWidth = len(headerTable) + 1

		context = {
			"allStudents":allStudents,
			"headerTable":headerTable,
			"headerWidth":headerWidth,
			"szczegoly": szczegoly,
			"klasa":klasa,
		}

	return render(request, "studentsInClass.html" , context)

def editStudent(request,id):
	if request.user.is_authenticated:
		if  request.user.is_expired():
			logout(request)
			HttpResponseRedirect('/')

		current_user = request.user

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		dbname = current_user.username
		dbname += ".sqlite3"
		conn = sqlite3.connect(os.path.join(BASE_DIR, 'userData', current_user.username + '.sqlite3'))
		c = conn.cursor()

		#Im getting class names from db from every student
		#then every unique class is appended to klasyPostfix which will contain
		#list of all existing classes
		query = "SELECT * FROM uczniowie"
		uczniowie = sqlDict(c, query)
		klasyPostfix = []
		klasyPostfix.append(("Brak","Brak"))

		for klasa in uczniowie['klasa']:
			if( not( choiceFieldTupleContains(klasyPostfix,klasa) ) and klasa != None ):
				klasyPostfix.append((klasa,klasa))

		# Edytowanie danych studenta klasy

		if "formEditStudent" in request.POST:
			EditStudent =  formEditStudent(request.POST,klasy=(klasyPostfix))
			if EditStudent.is_valid():
				imieUcznia = EditStudent.cleaned_data['imie']
				nazwiskoUcznia = EditStudent.cleaned_data['nazwisko']
				peselUcznia = EditStudent.cleaned_data['pesel']
				kod1Ucznia = EditStudent.cleaned_data['kod1']
				kod2Ucznia = EditStudent.cleaned_data['kod2']
				kodUcznia1 = kod1Ucznia +'-'+ kod2Ucznia
				miejscowoscUcznia = EditStudent.cleaned_data['miejscowosc']
				ulicaUcznia = EditStudent.cleaned_data['ulica']
				nrbudynkuUcznia = EditStudent.cleaned_data['nrbudynku']
				nrmieszkaniaUcznia = EditStudent.cleaned_data['nrmieszkania']
				kod12Ucznia = EditStudent.cleaned_data['kod12']
				kod22Ucznia = EditStudent.cleaned_data['kod22']
				kodUcznia2 = kod12Ucznia +'-'+ kod22Ucznia
				miejscowosc2Ucznia = EditStudent.cleaned_data['miejscowosc2']
				ulica2Ucznia = EditStudent.cleaned_data['ulica2']
				nrbudynku2Ucznia = EditStudent.cleaned_data['nrbudynku2']
				nrmieszkania2Ucznia = EditStudent.cleaned_data['nrmieszkania2']
				ocenaPolskiUcznia = EditStudent.cleaned_data['ocenPol']
				ocenaMatematykaUcznia = EditStudent.cleaned_data['ocenMat']
				ocenaAngielskiUcznia = EditStudent.cleaned_data['ocenAng']
				ocenaNiemieckiUcznia = EditStudent.cleaned_data['ocenNiem']
				klasaucznia = EditStudent.cleaned_data['klasa']
				iducznia = EditStudent.cleaned_data['iducznia']


				query = "SELECT * FROM uczniowie WHERE id={}".format(id)
				uczen = sqlDict(c, query)

				if(len(uczen['id']) == 1):
					if(uczen['klasa'][0] != klasaucznia):
						klasaOld = uczen['klasa'][0]
						if(klasaOld != None and klasaOld != 'Brak'):
							c.execute("DELETE FROM {} WHERE iducznia={}".format(klasaOld,id))
							conn.commit()
						if(klasaucznia != None and klasaucznia != 'Brak'):
							query = "INSERT INTO {} (iducznia,Imię,Nazwisko) VALUES('{}','{}','{}')".format(klasaucznia,iducznia,imieUcznia,nazwiskoUcznia)
							c.execute(query)
							conn.commit()
				if(klasaucznia != None and klasaucznia != 'Brak'):
					query = "UPDATE uczniowie SET Imię='{}',Nazwisko='{}',Pesel='{}',Kod_pocztowy='{}',Miejscowość='{}',Ulica='{}',Nr_budynku='{}',Nr_mieszkania='{}',Kod_pocztowy2='{}',Miejscowość2='{}',Ulica2='{}',Nr_budynku2='{}',Nr_mieszkania2='{}',polski='{}',matematyka='{}',angielski='{}',niemiecki='{}',klasa='{}' WHERE id={}".format(imieUcznia,nazwiskoUcznia,peselUcznia,kodUcznia1,miejscowoscUcznia,ulicaUcznia,nrbudynkuUcznia,nrmieszkaniaUcznia,kodUcznia2,miejscowosc2Ucznia,ulica2Ucznia,nrbudynku2Ucznia,nrmieszkania2Ucznia,ocenaMatematykaUcznia,ocenaPolskiUcznia,ocenaAngielskiUcznia,ocenaNiemieckiUcznia,klasaucznia,iducznia)
				else:
					query = "UPDATE uczniowie SET Imię='{}',Nazwisko='{}',Pesel='{}',Kod_pocztowy='{}',Miejscowość='{}',Ulica='{}',Nr_budynku='{}',Nr_mieszkania='{}',Kod_pocztowy2='{}',Miejscowość2='{}',Ulica2='{}',Nr_budynku2='{}',Nr_mieszkania2='{}',polski='{}',matematyka='{}',angielski='{}',niemiecki='{}',klasa=NULL WHERE id={}".format(imieUcznia,nazwiskoUcznia,peselUcznia,kodUcznia1,miejscowoscUcznia,ulicaUcznia,nrbudynkuUcznia,nrmieszkaniaUcznia,kodUcznia2,miejscowosc2Ucznia,ulica2Ucznia,nrbudynku2Ucznia,nrmieszkania2Ucznia,ocenaMatematykaUcznia,ocenaPolskiUcznia,ocenaAngielskiUcznia,ocenaNiemieckiUcznia,iducznia)
				c.execute(query)
				conn.commit()

		query = "SELECT * FROM uczniowie WHERE id="+id
		student = sqlDict(c, query)

		try:
			kod = student['Kod_pocztowy'][0].split("-")
		except:
			kod = []
			kod.append(None)
			kod.append(None)
		try:
			kod2 = student['Kod_pocztowy2'][0].split("-")
		except:
			kod2 = []
			kod2.append(None)
			kod2.append(None)

		instanceEditStudent=formEditStudent(initial={
			'imie':student['Imię'][0],
			'nazwisko':student['Nazwisko'][0],
			'pesel':student['Pesel'][0],
			'kod1':kod[0],
			'kod2':kod[1],
			'miejscowosc':student['Miejscowość'][0],
			'ulica':student['Ulica'][0],
			'nrbudynku':student['Nr_budynku'][0],
			'nrmieszkania':student['Nr_mieszkania'][0],
			'kod12':kod2[0],
			'kod22':kod2[1],
			'miejscowosc2':student['Miejscowość2'][0],
			'ulica2':student['Ulica2'][0],
			'nrbudynku2':student['Nr_budynku2'][0],
			'nrmieszkania2':student['Nr_mieszkania2'][0],
			'ocenPol':student['polski'][0],
			'ocenMat':student['matematyka'][0],
			'ocenAng':student['angielski'][0],
			'ocenNiem':student['niemiecki'][0],
			'klasa':student['klasa'][0],
			'iducznia':id
			},klasy=(klasyPostfix))

		context = {
			"formEditStudent":instanceEditStudent,
		}

		conn.close()

		return render(request, "editStudent.html" , context)

	return render(request, "editStudent.html" , context)

def deleteWholeClass(request, klasa):
	if request.user.is_authenticated:
		if  request.user.is_expired():
			logout(request)
			HttpResponseRedirect('/')

		current_user = request.user

		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		dbname = current_user.username
		dbname += ".sqlite3"
		conn = sqlite3.connect(os.path.join(BASE_DIR, 'userData', current_user.username + '.sqlite3'))
		c = conn.cursor()

		c.execute("DROP TABLE "+klasa)
		c.execute("UPDATE uczniowie SET klasa=NULL WHERE klasa='{}'".format(klasa))
		conn.commit()

		return HttpResponseRedirect("/sms/extended")

	return HttpResponseRedirect("/")
