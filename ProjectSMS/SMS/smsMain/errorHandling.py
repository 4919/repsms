import datetime, os
# Logs errors to file in BASE_DIR\error.log
# and then appends errors to session variable
# IMPORTANT : errors must be a interable object
def handleError(user,form=0):
	BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	errorlog = open(os.path.join(BASE_DIR,"error.log"), 'a')
	now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
	if form != 0:
		errorset = str(form.errors.as_data())
	else:
		errorset = {}
	errorlog.write(now+" "+user+" "+errorset+"\n")
	errorlog.close()
