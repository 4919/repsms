from django.shortcuts import render, redirect
from .forms import logowanie , rejestracja, kontakt , ChangePassword
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.contrib.auth.hashers import make_password
from .models import User
from django.contrib.auth import authenticate, login, logout
from django.utils.translation import ugettext_lazy as _
import string, random, sqlite3, smtplib,  os, urllib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import timedelta , datetime

# Produces .../repsms/ProjectSMS/SMS path
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def RandomString(size = 8, chars=string.ascii_letters + string.digits):
	return ''.join(random.SystemRandom().choice(chars) for i in range(size))
			
def index(request,typeMethod=None,activeid=None):
	# zmienna wyswietlajaca komunikat inforujacy o pomyslnej rejestraci i wysylajkaca email
	isregister=False
	# zmienna gotowy do logowania
	isReady=False
	# zmienna aktywujaca modal do zmiany hasla 
	isAboutToChangePass=False
	# flaga zapalajaca sie gdy pomyslnie zmieniono haslo
	passwdchanged=False
	# flaga zapalona jezeli podano poprawny email w przypomnij haslo
	if 'emailSendPwChange' in request.session:
		emailSendPwChange = request.session['emailSendPwChange']
		request.session['emailSendPwChange'] = False
	else:
		emailSendPwChange = False
	# Tworznie formularzy
	# Potwierzdzenie rejestracji  lub zmiana hasła
	if typeMethod == "activate":
		if(activeid !=None):
			try:
				conn = sqlite3.connect(os.path.join(BASE_DIR,"TempDB",'TemporaryUser.db'))
				c= conn.cursor()
				c.execute('SELECT * FROM tempUsers WHERE activCode=(?) ' ,[activeid])
				Users = c.fetchall()

				if(len(Users) == 1 ):
					
					User.objects._create_user(username=Users[0][0],
										 nazwaSzkoly=Users[0][1],
		                                 email=Users[0][2],
		                                 password=Users[0][3],
		                                 phoneNumber=Users[0][4],
		                                 )
					c.execute('DELETE FROM tempUsers WHERE activCode=(?) ' ,[activeid])

				conn.commit()
				conn.close()
				# Tworzenie potzrebnych tabel
				
				conn = sqlite3.connect(os.path.join(BASE_DIR,"userData", Users[0][0] + '.sqlite3'))

				c= conn.cursor()
				c.execute('CREATE TABLE IF NOT EXISTS algorytmy( id integer NOT NULL PRIMARY KEY AUTOINCREMENT, nazwa text, jpolski integer, matematyka integer, jangielski integer, jniemiecki integer )')
				c.execute("INSERT INTO algorytmy('nazwa','jpolski','matematyka','jangielski','jniemiecki') VALUES(?,?,?,?,?)",('Brak','1','1','1','1'))
				c.execute("CREATE TABLE IF NOT EXISTS uczniowie(id integer NOT NULL PRIMARY KEY AUTOINCREMENT, Pesel text, Imię text, Nazwisko text , Kod_pocztowy text, Miejscowość text, Ulica text, Nr_budynku text, Nr_mieszkania text, Kod_pocztowy2 text, Miejscowość2 text, Ulica2 text, Nr_budynku2 text, Nr_mieszkania2 text, polski text, angielski text, niemiecki text, francuski text, wloski text, hiszpanski text,rosyjski text, matematyka text, fizyka text, informatyka text, historia text, biologia text, chemia text, geografia text, wos text, zajęcia_techniczne text, zajęcia_artstyczne text, edukacja_dla_bezpieczeństwa text, plastyka text, muzyka text, wf text, zachowanie text, klasa text, UNIQUE(Pesel) ON CONFLICT REPLACE)")
				c.execute("CREATE TABLE IF NOT EXISTS kopie(id integer NOT NULL PRIMARY KEY AUTOINCREMENT,Nazwa text,Data_utworzenia DATE)")
				if not os.path.exists(os.path.join(BASE_DIR,"userData","copy", Users[0][0])):
					os.makedirs(os.path.join(BASE_DIR,"userData","copy", Users[0][0]))
				conn.commit()
				conn.close()
				isReady=True
			except:
				raise Http404("Coś poszło nie tak :(")

			# return redirect('/',isReady=True)
	elif typeMethod == "changepassword":
		isAboutToChangePass=True

	# obsluzenie requestu zmiany hasła 
	if "changepass" in request.POST:
		instance = ChangePassword(request.POST or None,initial=request.POST)
		if instance.is_valid():
			print("chpw valid")
			key = instance.cleaned_data['superKey']
			password = instance.cleaned_data['password']
			conn=sqlite3.connect(os.path.join(BASE_DIR,"TempDB","UserTempChangePassword.db"))
			c = conn.cursor()
			c.execute("SELECT email FROM User WHERE key =?", [key])
			row = c.fetchall()
			email = row[0]
			obj = User.objects.get(email = email[0])#User.objects.get(email=email)
			obj.password=make_password(password=password,salt=None,hasher='pbkdf2_sha1')
			c.execute("DELETE FROM User Where email =?",[email[0]])
			conn.commit()
			conn.close()
			obj.save()
			#isReady=True
			passwdchanged=True
		else:
			isAboutToChangePass=True


	# formluarz logowania
	instanceLogowanie=logowanie()
	# formularz rejestracji
	# label_suffix wyrzuca ten jebany dwukropek
	instanceRejestracja=rejestracja(initial=request.POST, label_suffix='')
	# formularz zmiany hasla 
	instanceChangePass=ChangePassword()
	# Jesli jest zalogowany to wpisz do kontaktu jego email
	if(request.user.is_authenticated):
		instancekontakt=kontakt(initial={'email': request.user.email})
	else:
		instancekontakt=kontakt()

	# itemcarusel to zmeinna trzymajaca stan głownej karuzeli w razie wystapienia erroru
	itemCarousel=['item','item active','item']
	if request.method == 'POST':
		print("!!!POST")
		instanceLogowanie=logowanie()

		if "zaloguj" in request.POST:
			instanceLogowanie = logowanie(request.POST or None)
			if instanceLogowanie.is_valid():
				passw = instanceLogowanie.cleaned_data['haslo']
				loginU = instanceLogowanie.cleaned_data['login']
				 
			
				login(request,instanceLogowanie.userr)
				return HttpResponseRedirect('/')
			else:
				isReady=True

		elif "rejestracja" in request.POST:
			instanceRejestracja = rejestracja(request.POST or None)
			if instanceRejestracja.is_valid():

				preRegister(instanceRejestracja)
				isregister=True
			else:	
				itemCarousel=['item active','item','item']
			
		elif "kontakt" in request.POST:
			instancekontakt= kontakt(request.POST or None,initial=request.POST)
			if instancekontakt.is_valid():
				imie = instancekontakt.cleaned_data['imie']
				nazwisko = instancekontakt.cleaned_data['nazwisko']
				email = instancekontakt.cleaned_data['email']
				tresc = instancekontakt.cleaned_data['tresc']
				connContact = sqlite3.connect(os.path.join(BASE_DIR,'contact.db'))
				contactCursor = connContact.cursor()
				contactCursor.execute("CREATE TABLE IF NOT EXISTS contact(imie text, nazwisko text, email text, tresc text)")
				contactCursor.execute("INSERT INTO contact('imie','nazwisko','email',tresc) VALUES(?,?,?,?)",(imie,nazwisko,email,tresc))
				connContact.commit()
				connContact.close()
				return HttpResponseRedirect("/")
			itemCarousel=['item','item','item active']

	context={
		"title":"SMS",
		"instanceL":instanceLogowanie,
		"instanceR":instanceRejestracja,
		"instanceC":instancekontakt,
		"incanceP":instanceChangePass,
		"itemCarousel":itemCarousel,
		"isregister":isregister,
		"isChangePassword":isAboutToChangePass,
		"isReady":isReady,
		"passwdchanged":passwdchanged,
		"emailSendPwChange":emailSendPwChange,
	 }
	return render (request, "index.html", context)



def confirm(request):
	return render (request, "ConfirmRegister.html", {})

def remember(request):	
	email=request.GET['email']
	email = urllib.parse.unquote(email)
	if len(str(email)) == 0:	
		return HttpResponseRedirect('/')

	try:
		instance = User.objects.get(email=email) # will raise exception
		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		conn=sqlite3.connect(os.path.join(BASE_DIR,"TempDB","UserTempChangePassword.db"))
		c=conn.cursor()
		c.execute("CREATE TABLE IF NOT EXISTS User(email text, key text)")
		while True:
			key=RandomString(size=32)
			c.execute('SELECT * FROM User WHERE key=(?) ' ,[key])
			if len(c.fetchall()) is 0:
				break
		c.execute("DELETE FROM User WHERE email=?",[email])
		c.execute("INSERT INTO User('email','key') VALUES(?,?)",(email,key))
		conn.commit()
		conn.close()
		# base data to send email  in my own email sender fun
		me = "repsms6@gmail.com"
		you = email
		src=os.path.join('templates','emailpassword.html')
		title='SMS przypomnienie hasła'
		BASE_DIR=os.path.join(BASE_DIR,src)
		content = open(BASE_DIR, 'r',encoding="utf-8").read()
		content = content.replace("**user**",instance.username)
		content = content.replace("**pass**",key)
		SendEmail(me,you,title,plain=None,html=content)
		request.session['emailSendPwChange'] = True
	except User.DoesNotExist as e:
		print("Exception raised! in platforma::views::remember function")
		print("Email does not exist")
	except:
		print("Exception raised! in platforma::views::remember function")
		print(e.__class__.__name__)
		return HttpResponseRedirect('/')
	return HttpResponseRedirect('/')
def OWnLogout(request):
	logout(request)
	return HttpResponseRedirect('/')

def SendEmail(me,you,title, **kwargs):
	
	msg = MIMEMultipart('alternative')
	msg['Subject'] = title
	msg['From'] = me
	msg['To'] = you
	msg['Content-Type'] = "text/html; charset=utf-8"

	if kwargs['plain'] is not None:
		part1 = MIMEText(kwargs['plain'], 'plain',"utf-8")
		msg.attach(part1)
	if kwargs['html'] is not None:	
		part2 = MIMEText(kwargs['html'].encode('utf-8'), 'html', 'utf-8')
		
		msg.attach(part2)
	login="repsms6@gmail.com"
	password="zaq1@WSX"
	s = smtplib.SMTP('smtp.gmail.com:587')
	s.ehlo()
	s.starttls()
	s.login(login,password)
	s.sendmail(me, you, msg.as_string())
	s.quit()

def preRegister(Data):
	# create db if not exist
	
	conn = sqlite3.connect(os.path.join(BASE_DIR,"TempDB",'TemporaryUser.db'))
	tUser = conn.cursor()
	tUser.execute("CREATE TABLE IF NOT EXISTS tempUsers(username text, nazwaSzkoly text, email text, password text ,phoneNumber integer,created timestamp, activCode text,expired timestamp)")

	username=Data.cleaned_data['username']
	nazwaSzkoly=Data.cleaned_data['nazwaSzkoly']
	email=Data.cleaned_data['email']
	password=Data.cleaned_data['password']
	phoneNumber=Data.cleaned_data['phoneNumber']
	created=datetime.now()

	while True:
		activateCode=RandomString(size=32)
		tUser.execute('SELECT * FROM tempUsers WHERE activCode=(?) ' ,[activateCode])

		if len(tUser.fetchall()) is 0:
			break

	
	tUser.execute("INSERT INTO tempUsers ('username','nazwaSzkoly','email','password','phoneNumber','created','activCode','expired') VALUES(?,?,?,?,?,?,?,?)",
				(username,nazwaSzkoly,email,password,phoneNumber,created,activateCode,created + timedelta(days=365)))
	conn.commit()
	conn.close()
	me = "repsms6@gmail.com"
	you = email
	src=os.path.join('templates','rejestracja.html')
	title='Witamy w aplikacji SMS'
	content = open(os.path.join(BASE_DIR,src), 'r',encoding="utf-8").read()
	content = content.replace("**activ**",activateCode)

	SendEmail(me,you,title,plain=None,html=content)
	
def chooseVerion(request):
	return render(request, "choosePlatform.html")
